package helpers

import (
	"context"
	"gitlab.com/kamee/picverse/logger"
	"strconv"
)

const LimitItems = 15
const ImageChunk = 4

func GetPageLimit(page string) (int, int) {
	i, err := strconv.Atoi(page)
	if err != nil {
		logger.Error.Println(err)
		return -1, -1
	}

	offset := i*LimitItems - LimitItems

	return offset, LimitItems
}

func GetCurrentPage(ctx context.Context) int {
	var i int
	var err error
	page := ctx.Value("page")
	if page != nil {
		_page := page.(string)
		i, err = strconv.Atoi(_page)
		if err != nil {
			logger.Error.Println(err)
			return 1
		}
	}

	return i
}
