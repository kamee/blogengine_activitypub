package activitypub

import "gitlab.com/kamee/picverse/activitypub/vocabulary"

func CreateCollection(items_count int, items []map[string]interface{}) *vocabulary.Collection {
	return &vocabulary.Collection{
		Context:    vocabulary.ActivityStream,
		Type:       vocabulary.CollectionType,
		TotalItems: items_count,
		Items:      items,
	}
}
