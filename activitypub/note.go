package activitypub

import (
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"time"
)

func CreateNote(username, content, summary string, tag []*vocabulary.Tags, media_type string, attachment []string, attachment_content string) *vocabulary.Note {

	source := &vocabulary.Source{
		Content:   content,
		MediaType: "text/markdown",
	}

	object := &vocabulary.Object{
		ID:           vocabulary.NoteID(username),
		Type:         vocabulary.ActivityNote,
		AttributedTo: vocabulary.ActorId(username),
		Content:      content,
		MediaType:    media_type,
		Source:       source,
		Summary:      summary,
		Published:    time.Now().Format(time.RFC3339),
		To:           vocabulary.PublicActivity,
	}

	if len(tag) != 0 {
		object.Tag = tag
	}

	var vocab_tags []*vocabulary.Attachment
	for _, elem := range attachment {
		if elem != "" {
			elem_url := vocabulary.Host + "/" + elem
			_attachment := &vocabulary.Attachment{
				Type:      vocabulary.ImageType,
				Content:   attachment_content,
				Url:       elem_url,
				MediaType: "image/png",
			}

			vocab_tags = append(vocab_tags, _attachment)
		}
	}

	object.Attachment = vocab_tags

	note := &vocabulary.Note{
		Context: vocabulary.ActivityStream,
		ID:      vocabulary.NewID(username),
		Type:    vocabulary.CreateType,
		Actor:   vocabulary.ActorId(username),
		Object:  object,
	}

	return note
}
