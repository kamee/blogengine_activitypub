package activitypub

import (
	"bytes"
	"crypto"
	"crypto/rsa"
	"fmt"
	"github.com/go-fed/httpsig"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"gitlab.com/kamee/picverse/logger"
)

func Verify(r *http.Request) error {
	verifier, err := httpsig.NewVerifier(r)
	if err != nil {
		logger.Critical.Println(err)
		return err
	}

	priv := vocabulary.PrivateKey()
	pub := priv.(*rsa.PrivateKey).PublicKey
	var algo httpsig.Algorithm = httpsig.RSA_SHA256
	var pubKey crypto.PublicKey = &pub

	return verifier.Verify(pubKey, algo)
}

func Send(to, actor string, b []byte) (string, error) {
	postSigner, _, _ := httpsig.NewSigner([]httpsig.Algorithm{httpsig.RSA_SHA256}, "SHA-256",
		[]string{"(request-target)", "date", "host", "digest"},
		httpsig.Signature, 120)
	byteCopy := make([]byte, len(b))
	copy(byteCopy, b)
	buf := bytes.NewBuffer(byteCopy)

	iri, err := url.Parse(to)
	if err != nil {
		logger.Error.Println(err.Error())
	}

	req, err := http.NewRequest("POST", to, buf)
	req.Header.Add("Accept-Charset", "utf-8")
	req.Header.Add("Date", time.Now().UTC().Format("Mon, 02 Jan 2006 15:04:05")+" GMT")
	req.Header.Add("User-Agent", "picverse/0.0.0")
	req.Header.Add("Host", iri.Host)
	req.Header.Add("Accept", "application/activity+json; charset=utf-8")
	req.Header.Add("Content-Type", "application/activity+json; charset=utf-8")

	err = postSigner.SignRequest(vocabulary.PrivateKey(), vocabulary.PubkeyId(actor), req, byteCopy)
	if err := Verify(req); err != nil {
		err = fmt.Errorf("Cannot verify http signature, check private key and publec key id")
		logger.Critical.Println(err)
		return "401", err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to post to %s", err.Error())
		logger.Critical.Println(err)
		return "", err
	}

	defer resp.Body.Close()

	return resp.Status, err
}
