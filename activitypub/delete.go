package activitypub

import (
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
)

/*
{
	  "@context": "https://www.w3.org/ns/activitystreams",
	    "summary": "Sally deleted a note",
	      "type": "Delete",
	        "actor": {
			    "type": "Person",
			        "name": "Sally"
				  },
				    "object": "http://example.org/notes/1",
				      "origin": {
					          "type": "Collection",
						      "name": "Sally's Notes"
						        }
						}
*/

func CreateDelete(id string) *vocabulary.Delete {
	return &vocabulary.Delete{
		Context: "https://www.w3.org/ns/activitystreams",
		Type:    "Delete",
		Actor: &vocabulary.APActor{
			Type: "Person",
			Name: "https://e7dc-5-77-145-49.eu.ngrok.io/tzuk",
		},
		Object: id,
		To:     vocabulary.PublicActivity,
	}
}
